from django.urls import path
from projects.views import project_list_view
from projects.views import project_details_view
from projects.views import create_project_view

urlpatterns = [
    path("", project_list_view, name="list_projects"),
    path("<int:id>/", project_details_view, name="show_project"),
    path("create/", create_project_view, name="create_project"),
]
